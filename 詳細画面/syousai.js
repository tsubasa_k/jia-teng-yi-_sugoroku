/*変数宣言　p_komyu, p_asobi変数がp_ningenryoku, p_keikenに切り替わっています*/
var p_benkyo = 0; //勉強ポイント変数
var p_ningenryoku = 0; //人間力ポイント変数
var p_keiken = 0; //経験ポイント変数
var m_goukei0 = 0; //進んだマスの合計変数 （プレイヤー0）
var m_goukei2 = 0; //進んだマスの合計変数 （プレイヤー2）
var goal = 80; //ゴールまでの差マス変数
var charcter = 0; // 参加者４人が順番にさいころを振るので、番号を付与してあげる
var before_m = 0;
var before_m2 = 0;

//送られてきたデータを新たな変数に代入
/*//確認用
alert(p_benkyo + ' ' + p_keiken + ' ' + p_ningenryoku);
alert(getParam(p_benkyo + ' ' + p_keiken + ' ' + p_ningenryoku));
*/
p_benkyo = getParam('p_benkyo'); //URLのパラメータ取得し、変数に代入
p_benkyo = Number(p_benkyo); //変数を数値に変換
p_keiken = getParam('p_keiken');
p_keiken = Number(p_keiken);
p_ningenryoku = getParam('p_ningenryoku');
p_ningenryoku = Number(p_ningenryoku);
m_goukei0 = getParam('m_goukei0');
m_goukei0 = Number(m_goukei0);
load_flg = getParam('load_flg');
load_flg = Number(load_flg);
m_goukei2 = getParam('m_goukei2');
m_goukei2 = Number(m_goukei2);
before_m = getParam('before_m');
before_m = Number(before_m);
before_m2 = getParam('before_m2');
before_m2 = Number(before_m2);
charcter = getParam('charcter');
charcter = Number(charcter);


//<body>タグ読み込み時にすぐに実行
function nowload(){
  document.getElementById('benkyo').textContent = '勉強pt = ' + p_benkyo;
  document.getElementById('keiken').textContent = '経験pt = ' + p_keiken;
  document.getElementById('ningenryoku').textContent = '人間力pt = ' + p_ningenryoku;
  document.getElementById('sa_goal').textContent = 'ゴールまで後 ' + (goal-m_goukei0) + 'マス';
  
}


/*URLのパラメータ（プレイヤーデータ）を取得*/
/*参考… https://www-creators.com/archives/4463 */ 
function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


/*詳細画面を閉じる*/
function exitPage(){
    //if(m_goukei0 <= 30){
        window.location.href = '../盤面/banmen1.html?p_benkyo=' + p_benkyo + '&p_keiken=' + p_keiken + '&p_ningenryoku=' + p_ningenryoku + '&m_goukei0=' + m_goukei0
        + '&m_goukei2=' + m_goukei2 + '&before_m=' + before_m + '&before_m2=' + before_m2 + '&charcter=' + charcter;
    /*}
    else if(m_goukei0 <= 60){
        window.location.href = 'banmen2.html?p_benkyo=' + p_benkyo + '&p_keiken=' + p_keiken + '&p_ningenryoku=' + p_ningenryoku + '&m_goukei0=' + m_goukei0;
    }
   else{
        window.location.href = 'banmen3.html?p_benkyo=' + p_benkyo + '&p_keiken=' + p_keiken + '&p_ningenryoku=' + p_ningenryoku + '&m_goukei0=' + m_goukei0;
    }*/
}



