var p_benkyo = 0; //勉強ポイント変数
var p_ningenryoku = 0; //人間力ポイント変数
var p_keiken = 0; //経験ポイント変数
var m_goukei0 = 0; //進んだマスの合計変数 （プレイヤー0）
var m_goukei1 = 0; //進んだマスの合計変数 （プレイヤー1）
var m_goukei2 = 0; //進んだマスの合計変数 （プレイヤー2）
var m_goukei3 = 0; //進んだマスの合計変数 （プレイヤー3）
var goal = 70; //ゴールまでの差マス変数
var charcter = 0; // 参加者４人が順番にさいころを振るので、番号を付与してあげる
var event_sw = 0; //0 or 1でイベントマスに止まるかを判定

//送られてきたデータを新たな変数に代入
/*//確認用
alert(p_benkyo + ' ' + p_keiken + ' ' + p_ningenryoku);
alert(getParam(p_benkyo + ' ' + p_keiken + ' ' + p_ningenryoku));
*/
p_benkyo = getParam('p_benkyo'); //URLのパラメータ取得し、変数に代入
p_benkyo = Number(p_benkyo); //変数を数値に変換
p_keiken = getParam('p_keiken');
p_keiken = Number(p_keiken);
p_ningenryoku = getParam('p_ningenryoku');
p_ningenryoku = Number(p_ningenryoku);
m_goukei0 = getParam('m_goukei0');
m_goukei0 = Number(m_goukei0);

/*URLのパラメータ（プレイヤーデータ）を取得*/
/*参考… https://www-creators.com/archives/4463 */
function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
//紙吹雪の記述
particlesJS("particles-js",{
    "particles":{
      "number":{
        "value":1250,//この数値を変更すると紙吹雪の数が増減できる
        "density":{
          "enable":false,
          "value_area":400
        }
      },
      "color": {
          "value": ["#EA5532", "#F6AD3C", "#FFF33F", "#00A95F", "#00ADA9", "#00AFEC","#4D4398", "#E85298"]//紙吹雪の色の数を増やすことが出来る
      },
      "shape":{
        "type":"polygon",//形状はpolygonを指定
        "stroke":{
          "width":0,
        },
        "polygon":{
          "nb_sides":5//多角形の角の数
        }
        },
        "opacity":{
          "value":1,
          "random":false,
          "anim":{
            "enable":true,
            "speed":20,
            "opacity_min":0,
            "sync":false
          }
        },
        "size":{
          "value":5.305992965476349,
          "random":true,//サイズをランダムに
          "anim":{
            "enable":true,
            "speed":1.345709068776642,
            "size_min":0.8,
            "sync":false
          }
        },
        "line_linked":{
          "enable":false,
        },
        "move":{
          "enable":true,
        "speed":10,//この数値を小さくするとゆっくりな動きになる
        "direction":"bottom",//下に向かって落ちる
        "random":false,//動きはランダムにならないように
        "straight":false,//動きをとどめない
        "out_mode":"out",//画面の外に出るように描写
        "bounce":false,//跳ね返りなし
          "attract":{
            "enable":false,
            "rotateX":600,
            "rotateY":1200
          }
        }
      },
      "interactivity":{
        "detect_on":"canvas",
        "events":{
          "onhover":{
            "enable":false,
          },
          "onclick":{
            "enable":false,
          },
          "resize":true
        },
      },
      "retina_detect":true
    });
    particlesJS("particles-js",{
        "particles":{
          "number":{
            "value":1250,//この数値を変更すると紙吹雪の数が増減できる
            "density":{
              "enable":false,
              "value_area":400
            }
          },
          "color": {
              "value": ["#EA5532", "#F6AD3C", "#FFF33F", "#00A95F", "#00ADA9", "#00AFEC","#4D4398", "#E85298"]//紙吹雪の色の数を増やすことが出来る
          },
          "shape":{
            "type":"polygon",//形状はpolygonを指定
            "stroke":{
              "width":0,
            },
            "polygon":{
              "nb_sides":5//多角形の角の数
            }
            },
            "opacity":{
              "value":1,
              "random":false,
              "anim":{
                "enable":true,
                "speed":20,
                "opacity_min":0,
                "sync":false
              }
            },
            "size":{
              "value":5.305992965476349,
              "random":true,//サイズをランダムに
              "anim":{
                "enable":true,
                "speed":1.345709068776642,
                "size_min":0.8,
                "sync":false
              }
            },
            "line_linked":{
              "enable":false,
            },
            "move":{
              "enable":true,
            "speed":10,//この数値を小さくするとゆっくりな動きになる
            "direction":"bottom",//下に向かって落ちる
            "random":false,//動きはランダムにならないように
            "straight":false,//動きをとどめない
            "out_mode":"out",//画面の外に出るように描写
            "bounce":false,//跳ね返りなし
              "attract":{
                "enable":false,
                "rotateX":600,
                "rotateY":1200
              }
            }
          },
          "interactivity":{
            "detect_on":"canvas",
            "events":{
              "onhover":{
                "enable":false,
              },
              "onclick":{
                "enable":false,
              },
              "resize":true
            },
          },
          "retina_detect":true
        });


