/*var script = document.createElement('script'); //変数名は適当なものにでも
script.src = "event_masu.js"; //ファイルパス
document.head.appendChild(script); //<head>に生成*/
var load_flg = 0;
// プレイヤー0
var p_benkyo = 0;       //勉強ポイント変数
var p_ningenryoku = 0;  //人間力ポイント変数
var p_keiken = 0;       //経験ポイント変数
var m_goukei0 = 0;      //進んだマスの合計変数
var goal = 80;         //ゴールまでの差マス変数
var before_m0 = 0;
var stop = 1;           // 一回休みの際に使う変数 stopが0の場合一回休み
var event_sw = 0;       //0 or 1でイベントマスに止まるかを判定

// プレイヤー１
var p_benkyo1 = 20;      //勉強ポイント変数
var p_ningenryoku1 = 0; //人間力ポイント変数
var p_keiken1 = 0;      //経験ポイント変数
var m_goukei1 = 0;      //進んだマスの合計変数
var goal1 = 80;         //ゴールまでの差マス変数
var before_m1 = 0;
var stop1 = 0;           // 一回休みの際に使う変数 stop1が1の場合一回休み
var event_sw1 = 0; //0 or 1でイベントマスに止まるかを判定

// プレイヤー切り替え
var charcter = 0; // 参加者４人が順番にさいころを振るので、番号を付与してあげる



//送られてきたデータを新たな変数に代入
//確認用
//alert(p_benkyo + ' ' + p_keiken + ' ' + p_ningenryoku);
//alert(getParam(p_benkyo + ' ' + p_keiken + ' ' + p_ningenryoku));
// プレイヤー０
p_benkyo = getParam('p_benkyo'); //URLのパラメータ取得し、変数に代入
p_benkyo = Number(p_benkyo); //変数を数値に変換
p_keiken = getParam('p_keiken');
p_keiken = Number(p_keiken);
p_ningenryoku = getParam('p_ningenryoku');
p_ningenryoku = Number(p_ningenryoku);
m_goukei0 = getParam('m_goukei0');
m_goukei0 = Number(m_goukei0);
// プレイヤー１
p_benkyo1 = getParam('p_benkyo1'); //URLのパラメータ取得し、変数に代入
p_benkyo1 = Number(p_benkyo1); //変数を数値に変換
p_keiken1 = getParam('p_keiken1');
p_keiken1 = Number(p_keiken1);
p_ningenryoku1 = getParam('p_ningenryoku1');
p_ningenryoku1 = Number(p_ningenryoku1);
m_goukei1 = getParam('m_goukei1');
m_goukei1 = Number(m_goukei1);




//<body>タグ読み込み時にすぐに実行
function firstLoading(){
  document.getElementById('benkyo').textContent = '勉強pt = ' + p_benkyo;
  document.getElementById('keiken').textContent = '経験pt = ' + p_keiken;
  document.getElementById('ningenryoku').textContent = '人間力pt = ' + p_ningenryoku;
  document.getElementById('sa_goal').textContent = 'ゴールまで後 ' + (goal-m_goukei0) + 'マス';
  document.getElementById('item').textContent = '持ち物： ' + (item[0]);
  //プレイヤー１ 
  document.getElementById('benkyo1').textContent = '勉強pt = ' + p_benkyo1;
  document.getElementById('keiken1').textContent = '経験pt = ' + p_keiken1;
  document.getElementById('ningenryoku1').textContent = '人間力pt = ' + p_ningenryoku1;
  document.getElementById('sa_goal1').textContent = 'ゴールまで後 ' + (goal1-m_goukei1) + 'マス';
  document.getElementById('item1').textContent = '持ち物： ' + (item1[0]);
}
/*
window.onload = function(){
  if(load_flg == 0){
    $id("btn").disabled = "true";
    const a = "加藤";
    if(!alert(a + "さんの初期値を自動で決めます。")){
      event_shake();
      p_benkyo += Math.ceil(event_sai / 2);
      p_keiken += Math.ceil(event_sai / 2);
      p_ningenryoku += Math.ceil(event_sai / 2);
      console.log(p_benkyo, p_keiken, p_ningenryoku, event_sai);
      player();
      setTimeout(event_0, 2000);
    }
  }
}

function event_0(){
    const b = "翼"
    if(!alert(b + "さんの初期値を自動で決めます。")){
      event_shake();
      p_benkyo1 += Math.ceil(event_sai / 2);
      p_keiken1 += Math.ceil(event_sai / 2);
      p_ningenryoku1 += Math.ceil(event_sai / 2);
      console.log(p_benkyo1, p_keiken1, p_ningenryoku1, event_sai);
      player();
      $id("btn").disabled = "";
    }
}
// window.onload = onLoad();

//詳細画面遷移用
function syousai_send(){
  window.location.href = '../詳細画面/syousai.html?p_benkyo=' + p_benkyo + '&p_keiken=' + p_keiken + '&p_ningenryoku=' + p_ningenryoku + '&m_goukei0=' + m_goukei0;
}
*/
/*URLのパラメータ（プレイヤーデータ）を取得*/
/*参考… https://www-creators.com/archives/4463 */ 
function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/*サイコロを振るプログラム */
var count;  // 変化しているように見せる回数
var $id = function(id){ return document.getElementById(id); };

/*サイコロを振る*/
function shake(){
  var sai = Math.floor(Math.random() * 6) + 1; // 1から6までの適当な数字
  saimg = sai + ".png";  // 画像ファイル名生成
  before_m = m_goukei0;
  before_m1 = m_goukei1;
  $id("saikoro").innerHTML = "<img src='" + saimg + "' width='100' height='100'>";
  $id("btn").disabled = "true";   // ボタンを使用不可にする
  sai = 83;
  if(charcter == 0){
    m_goukei0 += sai; //さいころの出目分加算（プレイヤー0）
    event_stop();     //イベントマスで必ず止まる処理
  }else if(charcter == 1){
    m_goukei1 += sai; //さいころの出目分加算（プレイヤー1）
    event_stop1();     //イベントマスで必ず止まる処理
  }
  
  
//  action(); //m_goukei0の位置に駒を進める
  koma_position();
  event_masu();
//  page(); //ページの切り替え
  player(); //プレイヤー情報表示
  change(); // playerとcpuの順番を交代
}

/*サイコロを振るときのアニメーション
function anime(){
  if(  > 20){  // 適当に20回ほど振る
    count = 0;
    $id("btn").disabled = "";   // ボタンを使える状態にする
    return 0;
  }
  shake();
  count++;
  setTimeout(anime, 50);  // 50ミリ秒間隔で表示切り替え
}
function saikoro(){
    count = 0;
    $id("btn").disabled = "true";   // ボタンを使用不可にする
    anime();                        // サイコロアニメ開始
}
*/

/*起動時の処理 このままだと　駒が進んでしまう
window.onload = function(){
    shake();        // 一回だけサイコロを振る
}
*/

/*プレイヤー情報の変更*/
function player(){
  document.getElementById('benkyo').textContent = '勉強pt = ' + p_benkyo;
  document.getElementById('keiken').textContent = '経験pt = ' + p_keiken;
  document.getElementById('ningenryoku').textContent = '人間力pt = ' + p_ningenryoku;
  document.getElementById('sa_goal').textContent = 'ゴールまで後 ' + (goal-m_goukei0) + 'マス';
  document.getElementById('item').textContent = '持ち物： ' + (item0[0]);
  //プレイヤー１ 
  document.getElementById('benkyo1').textContent = '勉強pt = ' + p_benkyo1;
  document.getElementById('keiken1').textContent = '経験pt = ' + p_keiken1;
  document.getElementById('ningenryoku1').textContent = '人間力pt = ' + p_ningenryoku1;
  document.getElementById('sa_goal1').textContent = 'ゴールまで後 ' + (goal1-m_goukei1) + 'マス'; 
  document.getElementById('item1').textContent = '持ち物： ' + (item1[0]);
}

/*ページの切り替え (+ 次の盤面へデータを送る)*/
function page(){
  //if(m_goukei0 >= 30){
	//	window.location.href = '../ゴール画面/goal.php?p_benkyo=' + p_benkyo + '&p_keiken=' + p_keiken + '&p_ningenryoku=' + p_ningenryoku + '&m_goukei0=' + m_goukei0;
  //}
	/*
	else if(m_goukei0 >= 60){
		window.location.href = 'banmen3.html?p_benkyo=' + p_benkyo + '&p_keiken=' + p_keiken + '&p_ningenryoku=' + p_ningenryoku + '&m_goukei0=' + m_goukei0;
	}
  else if(m_goukei0 >= 30){
		window.location.href = 'banmen2.html?p_benkyo=' + p_benkyo + '&p_keiken=' + p_keiken + '&p_ningenryoku=' + p_ningenryoku + '&m_goukei0=' + m_goukei0;
	}
	/*else if(m_goukei0 >= 30){ // いったんプレイヤーのみ動かしているので m_gokei0
		window.location.href = 'banmen2.html?m_gokei0=' + m_goukei0; // ?以降のコードでページ遷移後にも値を引き継ぐ
    m_goukei0 -= 20; // コマの位置を画面の場所(px)によって管理しているので、整数化した後に m_gokei から20(一ページ内のマス数)を引く
	}*/
}

/*プレイヤー0～4の順番を変更*/

function change(){
  if(charcter == 0){                // プレイヤーの処理が終わったら
    // モーダルウィンドウをクリックしたら次の処理を実行するPromiseという構文 関連：非同期処理 async/await
    // https://teratail.com/questions/277655
    const WaitForClick = () => new Promise(resolve => document.getElementById("close-" + (m_goukei0)).addEventListener("click", resolve));
    async function Wait_Next() {
      console.log("クリックするまで処理を止めます。");

      await WaitForClick();         // Promise処理の結果が返ってくるまで一時停止
      if(stop1 == 1 && event_effect0){
        if(!alert("プレイヤー1は、１回休みです。")){
          setTimeout(event0, 1000);
          charcter = 0;   // 次はプレイヤー1の番
          stop1 = 0;
          $id("btn").disabled = "";
        }
      }else if(stop1 == 1){
        alert("「イベント」赤点なので、プレイヤー1は、１回休みです。");
        charcter = 0;   // 次はプレイヤー0の番
        stop1 = 0;
        $id("btn").disabled = "";
      }else if(stop1 == 0){
        charcter = 1;
        if (m_goukei1 >= 83) charcter = 0;
        if(event_effect0){
          setTimeout(event0, 1000);
        }else if(m_goukei1 == 47 ){
          setTimeout(event_masu, 1000);
        }else $id("btn").disabled = "";
      } 
    }
    Wait_Next();
    skip0();
  }else if(charcter == 1){        // プレイヤー1の処理が終わったら
    const WaitForClick = () => new Promise(resolve => document.getElementById("close-" + (m_goukei1)).addEventListener("click", resolve));
    async function Wait_Next() {
      console.log("クリックするまで処理を止めます。");
      await WaitForClick();
      if(stop == 0 && event_effect1){
        if(!alert("「イベント」赤点なので、プレイヤー0は、１回休みです。")){
          setTimeout(event1, 1000);
          charcter = 1;   // 次はプレイヤー1の番
          stop = 1;
          $id("btn").disabled = "";
        }
      }else if(stop == 0){
        alert("プレイヤー0は、１回休みです。");
        charcter = 1;   // 次はプレイヤー1の番
        stop = 1;
        $id("btn").disabled = "";
      }else if(stop == 1){
        charcter = 0; // ここで交代させないといけない
        if (m_goukei0 >= 83) charcter = 1;
        if(event_effect1){
          setTimeout(event1, 100);
        }else if(m_goukei0 == 47 ){
          setTimeout(event_masu, 1000);
        }else $id("btn").disabled = "";
      } 
    }
    Wait_Next();
    skip1(); 
  }
}

function skip0(){ // [プレイヤー0]が一回休みになったときの処理 未完成
  if(stop == 0){
    if(m_goukei0 == 10){ // 10マス目のイベントマスで一回休みになった場合の処理
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo += Math.ceil(event_sai / 2);
    }
    else if(m_goukei0 == 29){
      // メッセージ出力 必要なし。ここには、モーダル出現と同時にメッセージが必要なら追加
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo += Math.ceil(event_sai / 2);
    }
    else if(m_goukei0 == 57){ // 57マス目のイベントマスで一回休みになった場合の処理
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo += Math.ceil(event_sai / 2);
    }
    else if(m_goukei0 == 76){
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo += Math.ceil(event_sai / 2);
    }
  }
}

function skip1(){ // [プレイヤー1]が一回休みになったときの処理 未完成
  if(stop1 == 1){
    if(m_goukei1 == 10){ // 10マス目のイベントマスで一回休みになった場合の処理
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo1 += Math.ceil(event_sai / 2);
    }
    else if(m_goukei1 == 29){
      // メッセージ出力 必要なし。ここには、モーダル出現と同時にメッセージが必要なら追加
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo1 += Math.ceil(event_sai / 2);
    }
    else if(m_goukei1 == 57){ // 57マス目のイベントマスで一回休みになった場合の処理
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo1 += Math.ceil(event_sai / 2);
    }
    else if(m_goukei1 == 76){
      $id("event_btn").disabled = false;//許可
      event_shake();
      p_benkyo1 += Math.ceil(event_sai / 2);
    }
  }
}

/*駒の位置移動 + マスに停止した際のモーダル出現*/ //未完成
function koma_position(){
  //event_stop();

  switch(charcter){
    case 0:
    circle = 'circle0';   // ローカル変数に駒の情報を付与
    var el = document.getElementById(circle);   // htmlの駒idを指定
    if(0<=before_m0 && before_m0<=12) //m_goukei0=0~15なら1段目
      m_top = 255;          // 駒を描画するときの配置
    else if(13<=before_m0 && before_m0<=26) //_goukei0=16~30なら2段目
      m_top = 392;          // 駒を描画するときの配置
    else if(27<=before_m0 && before_m0<=40) //m_goukei0=31~45なら3段目
      m_top = 529;          // 駒を描画するときの配置
    else if(41<=before_m0 && before_m0<=54) //m_goukei0=46~60なら4段目
      m_top = 666;          // 駒を描画するときの配置
    else if(55<=before_m0 && before_m0<=68) //m_goukei0=61~75なら5段目
      m_top = 803;          // 駒を描画するときの配置
    else if(69<=before_m0 && before_m0<=82) //m_goukei0=76~80なら6段目
      m_top = 940;          // 駒を描画するときの配置
    break;
    case 1:
      circle = 'circle1';
      el = document.getElementById(circle);   // htmlの駒idを指定
      if(0<=before_m1 && before_m1<=12) //m_goukei0=0~15なら1段目
        m_top = 255;          // 駒を描画するときの配置
      else if(13<=before_m1 && before_m1<=26) //m_goukei0=16~30なら2段目
         m_top = 392;          // 駒を描画するときの配置
      else if(27<=before_m1 && before_m1<=40) //m_goukei0=31~45なら3段目
        m_top = 529;          // 駒を描画するときの配置
      else if(41<=before_m1 && before_m1<=54) //m_goukei0=46~60なら4段目
        m_top = 666;          // 駒を描画するときの配置
      else if(55<=before_m1 && before_m1<=68) //m_goukei0=61~75なら5段目
        m_top = 803;          // 駒を描画するときの配置
      else if(69<=before_m1 && before_m1<=82) //m_goukei0=76~80なら6段目
        m_top = 940;          // 駒を描画するときの配置
    break;
  }
    if(charcter == 0){
      if(before_m0<m_goukei0){
        before_m0++;
        m_left = before_m0%14*136;
        el.style.left = m_left+"px";
        el.style.top = m_top+"px";
        console.log(before_m0);
        setTimeout(koma_position, 100);
      }else{
        var modal = "#modal-" + m_goukei0;
        console.log(m_goukei0+'マス目');
        document.getElementById(modal).click();//　banmen.htmlにidを付けていないの止めときます。
      }
    }else if(charcter == 1){
      if(before_m1<m_goukei1){
        before_m1++;
        m_left = before_m1%14*136+72;
        el.style.left = m_left+"px";
        el.style.top = m_top+"px";
        setTimeout(koma_position, 100);
      }else{
        var modal = "#modal-" + m_goukei1;
        console.log(m_goukei1+'マス目');
        document.getElementById(modal).click();
      }
    }
  }